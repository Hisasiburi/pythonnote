from Crypto.PublicKey import RSA
import json

sk = None
data = dict()
with open('rsa.json', 'r') as f:
    data = json.load(f)
n = int(data['modulus'], 0)
e = int(data['publicExponent'], 0)
d = int(data['privateExponent'], 0)
sk = RSA.construct((n, e, d))
print(sk.e)
