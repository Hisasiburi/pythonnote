import urllib.request

def request(url, method, queryDict, bodyDict):
    if queryDict != None:
        url +="?"
        for query in queryDict: 
            url+= query + "=" + queryDict[query] + "&"
    if method != "get":
        data = ""
        for body in bodyDict:
            data+= body + "=" + bodyDict[body] + "&"
        req = urllib.request.Request(url, data.encode("utf-8"))
        header = "sessionid=a66oh5qalsho0nccun2m84oljbuh71tg"
        req.add_header("Cookie", header)
    else:
        req = urllib.request.Request(url)
    res = urllib.request.urlopen(req)
    statusCode = res.getcode()
    if statusCode == 200:
        body = res.read()
        print(body.decode("utf-8"))
    else:
        print("Err", statusCode)

bodyDict = {
    "processType" : "content",
    "sel_year" : "2017",
    "sel_term": "1",
    "sel_dept" : "110",
    "sel_course" : "%25",
    "sel_subject" : "%25",
    "sel_lang": "A"
}

request("https://ssogw6.kaist.ac.kr/totalOpeningCourse", "post", None, bodyDict)