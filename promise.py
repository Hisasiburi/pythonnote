from types import FunctionType

class Promise(object):
    def __init__(self, executor=None):
        self.length = 1
        self.resolved_value = ()
        self.rejected_value = ()
        self.executable = False
        if not executor:
            executor = self._executor
        self.executor = executor

    def _executor(self, resolve, reject):
        while not self.executable and not self.resolved_value and not self.rejected_value:pass
        if self.resolved_value:
            return resolve(*self.resolved_value)
        if self.rejected_value:
            return reject(*self.rejected_value)

    def _fulfillment(self, next_promise, onFulfilled):
        def func(*args, **kwargs):
            if not onFulfilled:
                return None
            if not isinstance(onFulfilled, FunctionType):
                raise Exception('Not a function')
            next_promise.resolved_value = (onFulfilled(*args, **kwargs),)
            return next_promise.resolved_value
        return func

    def _rejection_handler(self, next_promise, onRejected):
        def func(*args, **kwargs):
            if not onRejected:
                next_promise.rejected_value = args
            elif not isinstance(onRejected, FunctionType):
                raise Exception('Not a function')
            else:
                next_promise.rejected_value = (onRejected(*args, **kwargs),)
            return next_promise.rejected_value
        return func

    def then(self, onFulfilled, onRejected=None):
        self.executable = True
        next_promise = Promise()

        self.executor(self._fulfillment(next_promise, onFulfilled),
                        self._rejection_handler(next_promise, onRejected))
        return next_promise
        
    def catch(self, onRejected):
        self.executable = True
        next_promise = Promise()
        self.executor(self._fulfillment(next_promise, None),
                        self._rejection_handler(next_promise, onRejected))
        return next_promise

    @classmethod
    def all(cls, iterable):
        pass

    @classmethod
    def race(cls, iterable):
        pass

    @classmethod
    def resolve(cls, value):
        pass

    @classmethod
    def reject(cls, reason):
        pass

def task(resolve, reject):
    a = "resolve"
    if a == "resolve":
        resolve(a)
    else:
        reject(a)

test = Promise(task)
test.then(lambda a: a) \
    .then(lambda b: b) \
    .then(lambda c: print("resolve:", c)) \
    .catch(lambda c: print('rejected:', c))