def highBlockIteration(i, j, supb, subb, A, B):
    dia=0
    for ip in range(i, i+supb, subb):
        for jp in range(j, j+supb, subb):
            for ib in range(ip, ip+subb):
                for jb in range(jp, jp+subb):
                    if (ib != jb):
                        B[jb][ib] = A[ib][jb]
                        tup =(i,j,ip,jp,ib,jb)
                        if (checkDupl.count(tup)>0 ):
                            print("duplicate", tup)
                        else :
                            checkDupl.append(tup)
                    else :
                        temp = A[ib][jb]
                        dia=1
                        dia_col=ib
                if (dia ==1):
                    B[dia_col][dia_col] = temp
                    tup = (i,j,ip,jp,dia_col,dia_col)
                    if (checkDupl.count(tup)>0):
                            print("duplicate")
                    else :
                        checkDupl.append(tup)
                    dia=0
    return B

def reversedHighBlockIteration(i,j,supb,subb,A,B):
    dia=0
    for ip in range(i,i-supb,-subb):
        for jp in range(j, j-supb, -subb):
            for ib in range(ip, ip-subb, -1):
                for jb in range(jp, jp-subb, -1):
                    if (ib!=jb):
                        B[jb][ib] = A[ib][jb]
                        tup = (i,j,ip,jp,ib,jb)
                        origin = [item for item in checkDupl if item[4] == tup[4] and item[5]==tup[5]]
                        if (len(origin)>0 ):
                            print("duplicated", tup, "for", origin)
                        else :
                            checkDupl.append(tup)
                    else :
                        temp =A[ib][jb]
                        dia =1
                        dia_col =ib
                if(dia ==1):
                    B[dia_col][dia_col] = temp
                    tup = (i,j,ip,jp,dia_col, dia_col)
                    origin = [item for item in checkDupl if item[4] == tup[4] and item[5]==tup[5]]                    
                    if (len(origin)>0):
                            print("duplicate")
                    else :
                        checkDupl.append(tup)
                    dia=0
    return B


supb=8
subb=4
M=64
N=64
A=[]
for i in range(N):
    A.append([])
    for j in range(M):
        A[i].append(N*i+j)

B=[]
for i in range(M):
    B.append([])
    for j in range(N):
        B[i].append(M*i+j)

C=[]
for i in range(M):
    C.append([])
    for j in range(N):
        C[i].append(M*i+j)

global checkDupl
checkDupl=[]
print("DIGONAL>>>")
for a in range(0,M,supb):
    B = highBlockIteration(a,a,supb,subb,A,B)
print("FIRST>>>")
for i in range(0,N-supb,supb):
    for j in range(i+supb,M, supb):
        B = highBlockIteration(i,j,supb,subb,A,B)
print("SECOND>>>")
for i in range(N-1,0,-supb):
    for j in range(i-supb,-supb, -supb):
        B = reversedHighBlockIteration(i,j,supb,subb,A,B)
print("<<<<<<")

for i in range(N):
    for j in range(M):
        C[j][i]=A[i][j]
        if (B[j][i] != C[j][i]):
            print("missMatch>>>A(%d,%d)"%(i,j))