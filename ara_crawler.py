import time, requests
from datetime import datetime
from requests import cookies
import random

ara = {
    'BuySell': "http://ara.kaist.ac.kr/board/BuySell/",
    'Wanted': "http://ara.kaist.ac.kr/board/Wanted/"
}

session_id="560ff35c321ba2b35893f9c922076231"

search = ('모니터', '키보드', 'ram', 'gtx', '아이패드', '가습기', '자전거', '그램',
            'monitor', 'keyboard', 'ipad', '그래픽카드', '램', 'gram',
            '무접점', '정전용량', '정전 용량', '이어폰', '헤드폰', '헤드셋', 'earphone',
            'headphone', 'headset', '오픈형', '공기계', 'lg', '갤럭시', 'ssd', '코트',
            'coat', '쿨러', '받침대', '마우스')

def get_html(url, session_id):
    jar = cookies.RequestsCookieJar()
    jar.set('sessionid', session_id, domain='ara.kaist.ac.kr', path='/')
    r = requests.get(url, cookies=jar)
    return r.text

def parse(html):
    collection = []
    raw0 = html.split("<td class=\"title\">")
    for ele in raw0[1:]:
        raw1 = ele.split("</td>")
        raw2 = raw1[0].split('<span')[0].strip()
        collection.append(raw2.lower())
    return collection

if __name__=="__main__":
    already_seen=[]
    while True:
        html = get_html(ara['BuySell'], session_id)
        col = parse(html)
        for title in col:
            for s in search:
                if title.find(s) != -1 and title not in already_seen:
                    already_seen.append(title)
                    print("Detected: {} [{}]".format(title, datetime.now()))
        print('')
        time.sleep(15*60)
