import time, requests
from datetime import datetime
from requests import cookies
import random
import sys

url="https://kds.kaist.ac.kr/proxy/query_dormitory_count_new.do"
session_id="1D7392692E458C218595841A0E09D9FA"

def get_html(url, session_id):
    jar = cookies.RequestsCookieJar()
    jar.set('JSESSIONID', session_id, domain='kds.kaist.ac.kr', path='/')
    jar.set('evSSOCookie', "egqPiIhNIBITfg2xLai3OUNXftHIATg+ml2L33HgCjA=", domain='.kaist.ac.kr', path='/')
    jar.set('ObSSOCookie', "hRFJOlEfOEOQwLdtDK7UIKwLZ9vki3Ib2X6hbxLKVp9BzVTR2pUmKQ5jGa3qwDq53ut5MM6yBncAuInAXS4juyB%2BmlcO%2F%2BK%2F3ccUZVs0%2FyvceYgHzsVYCKMa6gGu2RzKC1eM42TPbpPIhYFsrXD89RUU4kGeYNzy2Y66StoHLJHlBwyXFlcZfQNTUtDP9wRq9ql39fByleStTMvA%2FUxxOAmkYx%2FRyz8eFN7QpyoNYtn0sZLigLifpR1tiUJrwyiUBaC6THcROvWSN9tcTHQRHJbjlVB9elEU%2FPw71nK42H0%3D", domain='.kaist.ac.kr', path='/')
    jar.set('SATHTOKEN', "82e9f23d117e9e30eb0bf13363054817c8e8d0291cb4ee1dfb47a7cbd8f15bf641704671e0ad15ba45cb92ac5b28c6d0c0a11b68a831f5fef969bb8ea0ffefbd", domain='.kaist.ac.kr', path='/')
    
    r = requests.post(url, cookies=jar, data = {
                                        'ks_dormitory_name':'문지관',
                                        'ks_room_kind': '717720002',
                                        'ks_room_capacity':2}, stream=True)
    return r


if __name__=="__main__":
    r = get_html(url, session_id)
    print(r.text.encode('ascii'))
    print(r.raw)
    
    with open('kds_room.txt', 'wb') as fd:
        for chunk in r.iter_content(chunk_size=1024, decode_unicode=False):
            fd.write(chunk)
