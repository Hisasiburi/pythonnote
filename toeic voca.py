import os
import time
import random
import ctypes
from ctypes import wintypes
# send keyboard signal for conversion between English and Korean #
user32 = ctypes.WinDLL('user32', use_last_error=True)
INPUT_MOUSE    = 0
INPUT_KEYBOARD = 1
INPUT_HARDWARE = 2
KEYEVENTF_EXTENDEDKEY = 0x0001
KEYEVENTF_KEYUP       = 0x0002
KEYEVENTF_UNICODE     = 0x0004
KEYEVENTF_SCANCODE    = 0x0008
MAPVK_VK_TO_VSC = 0
VK_HanYoung = 0x15
wintypes.ULONG_PTR = wintypes.WPARAM
class KEYBDINPUT(ctypes.Structure):
    _fields_ = (("wVk",         wintypes.WORD),
                ("wScan",       wintypes.WORD),
                ("dwFlags",     wintypes.DWORD),
                ("time",        wintypes.DWORD),
                ("dwExtraInfo", wintypes.ULONG_PTR))

    def __init__(self, *args, **kwds):
        super(KEYBDINPUT, self).__init__(*args, **kwds)
        # some programs use the scan code even if KEYEVENTF_SCANCODE
        # isn't set in dwFflags, so attempt to map the correct code.
        if not self.dwFlags & KEYEVENTF_UNICODE:
            self.wScan = user32.MapVirtualKeyExW(self.wVk,MAPVK_VK_TO_VSC, 0)
class MOUSEINPUT(ctypes.Structure):
    _fields_ = (("dx",          wintypes.LONG),
            ("dy",          wintypes.LONG),
            ("mouseData",   wintypes.DWORD),
            ("dwFlags",     wintypes.DWORD),
            ("time",        wintypes.DWORD),
            ("dwExtraInfo", wintypes.ULONG_PTR))
class HARDWAREINPUT(ctypes.Structure):
    _fields_ = (("uMsg",    wintypes.DWORD),
                ("wParamL", wintypes.WORD),
                ("wParamH", wintypes.WORD))
class INPUT(ctypes.Structure):
    class _INPUT(ctypes.Union):
        _fields_ = (("ki", KEYBDINPUT),
                    ("mi", MOUSEINPUT),
                    ("hi", HARDWAREINPUT))
    _anonymous_ = ("_input",)
    _fields_ = (("type",   wintypes.DWORD),
                ("_input", _INPUT))
LPINPUT = ctypes.POINTER(INPUT)

def _check_count(result, func, args):
    if result == 0:
        raise ctypes.WinError(ctypes.get_last_error())
    return args

user32.SendInput.errcheck = _check_count
user32.SendInput.argtypes = (wintypes.UINT, # nInputs
                             LPINPUT,       # pInputs
                             ctypes.c_int)  # cbSize
def PressKey(hexKeyCode):
    x = INPUT(type=INPUT_KEYBOARD,
              ki=KEYBDINPUT(wVk=hexKeyCode))
    user32.SendInput(1, ctypes.byref(x), ctypes.sizeof(x))
def ReleaseKey(hexKeyCode):
    x = INPUT(type=INPUT_KEYBOARD,
              ki=KEYBDINPUT(wVk=hexKeyCode,
                            dwFlags=KEYEVENTF_KEYUP))
    user32.SendInput(1, ctypes.byref(x), ctypes.sizeof(x))


word=None
default=1 # 0: some words added, 1: no words added
t=None

# Add new words
f=open('./toeic words.txt', 'a', encoding='UTF-8')
print("\nWecome to toeic voca practice program.")
print("\n**This program supports an automatic conversion of Korean-English.**")
print('**First, write down the toeic words to add, if not, just press enter key.**\n')
word=input('word>>>')
if word=='': # There is no words to add
    t=input('PRACTICE MODE\nHow much do you want to repeat?>>>')
    if not t.isdigit():
        t=None
    else:
        t=int(t)
        print('%d times'%(t))
else: # added some words
    default=0
    PressKey(VK_HanYoung)
    ReleaseKey(VK_HanYoung)
    meaning=input('meaning>>>')
    if meaning=='':
        meaning='NULL'
    f.write('0 '+word+'  '+meaning+'\n')
while word!='':
    PressKey(VK_HanYoung)
    ReleaseKey(VK_HanYoung)
    word=input('word>>>')
    if word=='':
        break
    default=0
    PressKey(VK_HanYoung)
    ReleaseKey(VK_HanYoung)
    meaning=input('meaning>>>')
    if meaning=='':
        meaning='NULL'
    f.write('0 '+word+'  '+meaning+'\n')
f.close()

# Practice with saved words
f=open('./toeic words.txt', 'r', encoding='UTF-8')
if os.path.getsize('./toeic words.txt') >0:
    db=[]
    for n,i in enumerate(f): # Load existed words into the db list
        fair=i.strip().split()
        add=int(fair.pop(0))
        fair.insert(0,add)
        db.append(fair)
    f.close()
    length=n
    begin=max(db)[0]
    meaning=None
    temp=None
    pre_WorM=0
    print("\nLET'S START")
    print("\n**If you want to delete corresponded word, type 'delete'.**")
    print("**If you don't know the answer, type '?'.**\n")
    while meaning!='exit':
        if max(db)[0]-begin==t and t!=None: # The target number(t) has been reached
            print('-You reviewed %d times\nlet me terminate the program.-'%(t))
            de=input("Do you want more? 'y' or 'n'>>>")
            if de=='n':
                print("\n-Thank you for using this program.-")
                time.sleep(0.5)
                break
            else:
                t=None
                continue
        if max(db)[0]-min(db)[0]<2 and default!=1 and not max(db)[0]<5: # The recommanded number for the added words has been reached
            end=input("You did a good job!\nkeep going? 'y' or 'n'.>>>")
            if end=='n':
                print("\n-Thank you for using this program.-")
                time.sleep(0.5)
                break
            elif end!='y':
                print('type correctly')
                continue
            default=1
        if temp==None: # Choose a word which has lowest score
            mini=min(db)[0]
            min_list=[]
            for i in db:
                if mini==i[0]:
                    min_list.append(i)
            fair=random.choice(min_list)
        else: # show again the incorrected word
            fair=temp
            temp=None
        
        # Randomly choose to show either word or its meaning  
        WorM=random.randint(1,2)
        if pre_WorM>0: # There is a word to be shown again
            WorM=pre_WorM
            pre_WorM-=2
        if pre_WorM%2 != WorM%2: # convert input language to another
            PressKey(VK_HanYoung)
            ReleaseKey(VK_HanYoung)
        #Answer#
        _input=input(fair[WorM]+'>>>')
        if _input=='delete': #request to delete#
            de=input("Do you really want to delete? 'y' or 'n'>>>")
            if de=='y':
                db.remove(fair)
                f=open('./toeic words.txt', 'w', encoding='UTF-8')
                for i in db:
                    f.write(str(i[0])+' '+i[1]+'  '+i[2]+'\n')
                f.close()
                print('DELETED')
            elif de!='n':
                print("I'm going to regard it 'NO'")
        elif _input=='exit': # request to terminate this program
            print("\n-Thank you for using this program.-")
            time.sleep(0.5)
            break
        elif _input=='?': # Have no idea
            print(fair[-1*WorM+3])
            pre_WorM=WorM
        elif _input!=fair[-1*WorM+3]: # incorrect
            print('No')
            temp=fair
            pre_WorM=WorM
        else: # correct
            db[db.index(fair)][0]+=1
            max_fair = max(db)
            min_fair = min(db)
            if max_fair[0] > 10 and min_fair[0] < 10:
                db[db.index(max_fair)][0]-=1
            f=open('./toeic words.txt', 'w', encoding='UTF-8')
            for i in db:
                f.write(str(i[0])+' '+i[1]+'  '+i[2]+'\n')
            f.close()
            pre_WorM=WorM-2
else: # There is no words to test
    f.close()
    print('***There is no words.***\nPlease, restart this program to add your words.')
    print("\n-Thank you for using this program.-")